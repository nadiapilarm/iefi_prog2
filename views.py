#views.py
import tkinter as tk
from tkinter import ttk, messagebox

class turnosExamenesView():
    def __init__(self, root):
        '''Constructor de la clase'''
        self.root = root
        self.root.title('Turnos de exámenes')

        self.frame1 = ttk.Frame(self.root, border=2, relief='groove')
        self.frame1.grid(padx=5, pady=5, row=0, column=0, sticky=tk.N+tk.S+tk.E+tk.W)

        self.labelId = ttk.Label(self.frame1, text='Id: ')
        self.labelId.grid(row=0, column=0, padx=5, pady=5)

        self.textvarId = tk.StringVar()
        self.entryId = ttk.Entry(self.frame1, textvariable=self.textvarId, state='disabled')
        self.entryId.grid(row=0, column=1, padx=5, pady=5)

        self.labelAppName = ttk.Label(self.frame1, text='Nombre App: ')
        self.labelAppName.grid(row=1, column=0, padx=5, pady=5, sticky='e')

        self.textvarAppName = tk.StringVar()
        self.entryAppName = ttk.Entry(self.frame1, textvariable=self.textvarAppName)
        self.entryAppName.grid(row=1, column=1, padx=5, pady=5)

        self.labelCuenta = ttk.Label(self.frame1, text='Cuenta: ')
        self.labelCuenta.grid(row=0, column=2, padx=5, pady=5, sticky='e')

        self.textvarCuenta = tk.StringVar()
        self.entryCuenta = ttk.Entry(self.frame1, textvariable=self.textvarCuenta)
        self.entryCuenta.grid(row=0, column=3, padx=5, pady=5)

        self.labelClave = ttk.Label(self.frame1, text='Clave: ')
        self.labelClave.grid(row=1, column=2, padx=5, pady=5, sticky=tk.E)

        self.textvarClave = tk.StringVar()
        self.entryClave = ttk.Entry(self.frame1, textvariable=self.textvarClave)
        self.entryClave.grid(row=1, column=3, padx=5, pady=5)

        
        self.frame2 = ttk.Frame(self.root, border=0)
        self.frame2.grid(padx=5, pady=5, row=1, column=0, sticky='nsew')

        #Aplicamos estilos y los modificamos para la cabecera del treeview.
        self.style = ttk.Style(self.root)
        self.style.theme_use('clam')
        self.style.configure("Treeview.Heading", background='pink', foreground='black')

        #Define el nombre y el ancho en píxeles de las columnas.
        columns = {'Id': 50, 'Nombre App': 200, 'Cuenta': 200, 'Clave': 110}
        self.treeview = ttk.Treeview(self.frame2, columns=tuple(columns.keys()), show='headings', height=14, selectmode='browse')
        #Define las cabeceras
        for clave, valor in columns.items():
            self.treeview.heading(clave, text=clave)
            self.treeview.column(clave, width=valor)

        self.treeview.grid(row=0, column=0, sticky='nsew')

        #Añade un barra lateral de scroll (enrollado).
        scrollbar = ttk.Scrollbar(self.frame2, orient=tk.VERTICAL, command=self.treeview.yview)
        #self.treeview.configure(yscroll=scrollbar.set)
        self.treeview.configure(yscrollcommand=scrollbar.set)
        scrollbar.grid(row=0, column=1, sticky='ns')

        self.frame3 = ttk.Frame(self.root, border=0)
        self.frame3.grid(padx=5, pady=5, row=2, column=0, sticky='nsesw')

        self.buttonAdd = ttk.Button(self.frame3, text='Añadir')
        self.buttonAdd.pack(side=tk.LEFT, padx=5, pady=5, fill=tk.X, expand=True)

        self.buttonUpdate = ttk.Button(self.frame3, text='Actualizar')
        self.buttonUpdate.pack(side=tk.LEFT, padx=5, pady=5, fill=tk.X, expand=True)

        self.buttonRemove = ttk.Button(self.frame3, text='Eliminar')
        self.buttonRemove.pack(side=tk.LEFT, padx=5, pady=5, fill=tk.X, expand=True)

        self.buttonClear = ttk.Button(self.frame3, text='Limpiar')
        self.buttonClear.pack(side=tk.LEFT, padx=5, pady=5, fill=tk.X, expand=True)

        self.frame4 = ttk.Frame(self.root, border=1, relief='groove')
        self.frame4.grid(padx=5, pady=5, row=3, column=0, columnspan=2, sticky=tk.N+tk.S+tk.E+tk.W)

        statusbar = tk.Label(self.frame4, text='Turnos exámenes', bd=1, relief=tk.SUNKEN, anchor=tk.W)
        statusbar.pack(side=tk.BOTTOM, fill=tk.X)

    def getId(self):
        '''getId Obtiene el valor de textvarId

        Returns:
            str: retorna el valor de textvarId
        '''
        return self.textvarId.get()

    def setId(self, id: str):
        '''setId establece el valor de textvarId.

        Args:
            id (str): valor de textvarId.
        '''
        self.textvarId.set(id)

    def getAppName(self):
        '''getAppName obtiene el valor de appName .

        Returns:
            str: retorna el valor de AppName.
        '''
        return self.textvarAppName.get()

    def setAppName(self, AppName):
        '''setAppName establece el valor de AppName.

        Args:
            AppName (_type_): _description_
        '''
        self.textvarAppName.set(AppName)

    def getCuenta(self):
        '''getCuenta devuelve el valor de Cuenta.

        Returns:
            str: devuelve el valor de Cuenta.
        '''
        return self.textvarCuenta.get()

    def setCuenta(self, cuenta: str):
        '''setCuenta establece el valor de Cuenta.

        Args:
            cuenta (str): nombre de la cuenta.
        '''
        self.textvarCuenta.set(cuenta)

    def getClave(self):
        return self.textvarClave.get()

    def setClave(self, clave):
        self.textvarClave.set(clave)


    def getCursorId(self) -> str:
        '''getCursorId obtiene el valor de Id del cursor.

        Returns:
            str: devuelve el valor de Id del cursor.
        '''
        selectedLbData = self.treeview.selection()[0]
        id = self.treeview.item(selectedLbData)['values'][0]
        return id

    def getCursorAppName(self) -> str:
        '''getCursorAppName obtiene el valor de appName del cursor.

        Returns:
            str: devuelve el valor de appName del cursor.
        '''
        selectedLbData = self.treeview.selection()[0]
        AppName = self.treeview.item(selectedLbData)['values'][1]
        return AppName

    def getCursorCuenta(self) -> str:
        selectedLbData = self.treeview.selection()[0]
        cuenta = self.treeview.item(selectedLbData)['values'][2]
        return cuenta

    def getCursorClave(self):
        selectedLbData = self.treeview.selection()[0]
        clave = self.treeview.item(selectedLbData)['values'][3]
        return clave


    def setTreeview(self, data: list):
        '''setTreeview pones datos en el treeview

        Args:
            data (list): recibe datos como lista.
        '''
        #Elimina los datos anteriores.
        self.treeview.delete(*self.treeview.get_children())

        #Sale si no trae datos y evita la asignación de datos vacios.
        if not data: return

        #Asigna los datos del data.
        for row in data:
            #Inserta una row en el treeview.
            self.treeview.insert('', tk.END, text=row[0], values=row)

    def showMessageBox(self, message: str, title: str, type: str):
        '''showMessageBox muestra un mensaje en una ventana emergente.

        Args:
            message (str): mensaje a mostrar.
            title (str): Titulo de la ventana.
            type (str): tipo de ventana.
        '''
        if type == 'info':
            messagebox.showinfo(title, message)
        elif type == 'warning':
            messagebox.showwarning(title, message)
        elif type == 'error':
            messagebox.showerror(title, message)
