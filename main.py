'''
Módulo principal, punto de entrada a la aplicación.
'''
import tkinter as tk

import controllers

from centrar_ventana import centrar

if __name__ == '__main__':
    root = tk.Tk()
    root.geometry(centrar(alto=475, ancho=600, app=root))
    root.resizable(True, True)

    controller = controllers.turnosExamenes(root)
    root.mainloop()
