#models.py
import dbConn

class turnosExamenesModel():
    '''turnosExamenesModel Modelo de la tabla turnosExamenes.
    '''
    def __init__(self):
        '''__init__ Constructor de la clase, establece la conexión a la base
        de datos e intenta crear la tabla si no existe.
        '''
        self.AppName = ''
        self.cuenta = ''
        self.clave = ''
        

        #Abre la conexión con la base de datos turnosExamenes y si no existe la crea.
        self.conn = dbConn.dbConn('turnosExamenes.sqlite3')
        #Crea la tabla turnosExamenes si no existe.
        tableName = 'turnosExamenes'
        fieldsDescripcion = '('\
            'id INTEGER PRIMARY KEY AUTOINCREMENT, '\
            'AppName TEXT NOT NULL, '\
            'cuenta TEXT NOT NULL, '\
            'clave TEXT NOT NULL)'
        #Ejecuta el comando en la base de datos.
        self.conn.createTable(tableName=tableName, fieldsDescripcion=fieldsDescripcion)

    def create(self, AppName: str, cuenta: str, clave: str) -> list:
        '''create crea un nuevo registro en la base de datos.

        Args:
            AppName (str): título de la aplicación.
            cuenta (str): cuenta.
            clave (str): clave.
            
        Returns:
            list: Devuelve el registro creado.
        '''
        command = 'INSERT INTO turnosExamenes (AppName, cuenta, clave) VALUES (?, ?, ?)'
        values = (AppName, cuenta, clave)
        return self.conn.execute(command, values)

    def read(self, id: int):
        '''read Lee un registro de la base de datos.

        Args:
            id (int): Id del registro a leer.

        Returns:
            list: Retorna el registro leído.
        '''
        command = 'SELECT * FROM turnosExamenes WHERE id = ?'
        values = (id,)
        return self.conn.execute(command, values)

    def update(self, id: str, AppName: str, cuenta: str, clave: str) -> list:
        '''update Actualiza un registro en la base de datos

        Args:
            id (str): Ide del registro a modificar.
            AppName (str): Nombre de la aplicación.
            cuenta (str): Cuenta.
            clave (str): Clave.
            
        Returns:
            list: Retorna el registro modificado.
        '''
        command = 'UPDATE turnosExamenes SET AppName = ?, cuenta = ?, clave = ? WHERE id = ?'
        values = (AppName, cuenta, clave, id)
        return self.conn.execute(command, values)

    def delete(self, id: str):
        '''delete Borra un registro de la base de datos.

        Args:
            id (str): Id del registro a borrar.

        Returns:
            list: Devuelve el registro borrado.
        '''
        command = 'DELETE FROM turnosExamenes WHERE id = ?'
        values = (id,)
        return self.conn.execute(command, values)

    def getAllData(self):
        '''getAllData Recupera todos los registros de la base de datos.

        Returns:
            list: devuelve una lista de turnosExamenes.
        '''
        command = 'SELECT * FROM turnosExamenes'
        return self.conn.execute(command)

    def __del__(self):
        '''__del__ Destructor de la clase.'''
        del self.conn
