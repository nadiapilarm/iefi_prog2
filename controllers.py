#controllers.py
import views
import models

class turnosExamenes():
    '''turnosExamenes se encarga de manejar la vista y el modelo.
    '''
    def __init__(self, root: object) -> None:
        '''__init__ Constructor de la clase.

        Args:
            root (object): Ventana de la aplicación.
        '''
        self.root = root
        # Instanciamos el modelo.
        self.model = models.turnosExamenesModel()
        # Instanciamos la vista.
        self.view = views.turnosExamenesView(root)
        # Añade la función addToTreeview al botón de agregar.
        self.view.buttonAdd["command"] = self.addToTreeview
        # Añade la función removeFromTreeview al botón de eliminar.
        self.view.buttonUpdate["command"] = self.updateFromTreeview
        # Añade la función updateFromTreeview al botón de actualizar.
        self.view.buttonRemove["command"] = self.removeFromTreeview
        # Añade la función loadTreeviewToEntry al evento de selección de fila.
        self.view.buttonClear["command"] = self.clearForm

        # Añade la función loadTreeviewToEntry al evento de selección de fila.
        self.view.treeview.bind('<<TreeviewSelect>>', self.loadTreeviewToEntry)
        # Carga los datos de la base de datos al treeview.
        self.loadToTreeview()

    def loadToTreeview(self):
        '''loadToTreeview Carga los datos de la base de datos al treeview.
        '''
        data = self.model.getAllData()
        self.view.setTreeview(data)

    def addToTreeview(self):
        '''addToTreeview Agrega un registro a la base de datos y al treeview.'''
        #Solo si hay un registro seleccionado el el treeview.
        if self.view.textvarCuenta.get() != '' and \
            self.view.textvarAppName.get() != '' and \
            self.view.textvarClave.get() != '':

            self.addToDB()
            self.loadToTreeview()
            self.clearForm()
        else:
            self.view.showMessageBox(message='Debe llenar todos los campos.', title='Error', type='error')

    def removeFromTreeview(self):
        '''removeFromTreeview Elimina un registro de la base de datos y del treeview.'''
        #Solo si hay un registro seleccionado el el treeview.
        if self.view.treeview.selection():
            self.removeFromDB()
            self.loadToTreeview()
            self.clearForm()

    def updateFromTreeview(self):
        '''updateFromTreeview Actualiza un registro de la base de datos y del treeview.'''
        #Solo si hay un registro seleccionado el el treeview.
        if self.view.treeview.selection():
            self.updateDB()
            self.loadToTreeview()
            self.clearForm()

    def loadTreeviewToEntry(self, event=None):
        '''loadTreeviewToEntry Carga los datos del treeview a los Entry.'''
        #Solo si hay un registro seleccionado el el treeview.
        if self.view.treeview.selection():
            id = self.view.getCursorId()
            AppName = self.view.getCursorAppName()
            cuenta = self.view.getCursorCuenta()
            clave = self.view.getCursorClave()
            
            self.view.setId(id)
            self.view.setAppName(AppName)
            self.view.setCuenta(cuenta)
            self.view.setClave(clave)
            

    def addToDB(self):
        '''addToDB Agrega un registro a la base de datos.'''
        AppName = self.view.getAppName()
        cuenta = self.view.getCuenta()
        clave = self.view.getClave()
        self.model.create(AppName, cuenta, clave)

    def updateDB(self):
        '''updateDB Actualiza un registro de la base de datos.'''
        id = self.view.getCursorId()
        AppName = self.view.getAppName()
        cuenta = self.view.getCuenta()
        clave = self.view.getClave()
        self.model.update(id, AppName, cuenta, clave)

    def removeFromDB(self):
        '''removeFromDB Elimina un registro de la base de datos.'''
        id = self.view.getCursorId()
        self.model.delete(id)

    def clearForm(self):
        '''clearForm Limpia los Entry del formulario.'''
        self.view.setId('')
        self.view.setAppName('')
        self.view.setCuenta('')
        self.view.setClave('')
        

        #Deselecciona fila de treeview.
        self.view.treeview.selection_remove(self.view.treeview.selection())

        return
